# Babel
![img](https://img.shields.io/badge/version-beta-orange.svg)


Babel is a web application using python and flask to keep track of my books.

## Prerequisites

This project needs python, virtualenv, virtualenvwrapper, pip, flask, mysql-server, Nginx and uWGSI.

## Installation

Clone the repository.

Set up a virtual environment and activate it.
	
	$ mkvirtualenv babelENV
	$ workon babelENV

Install project requirements using pip
	
	$ pip install -r requirements.txt

Run mysql-server and create a new user and database for the application.
	
	mysql -u root -p

	mysql> CREATE USER 'username'@'localhost' IDENTIFIED BY 'password';

	mysql> CREATE DATABASE babel_db;

	mysql> GRANT ALL PRIVILEGES ON babel_db . * TO 'username'@'localhost';

Configure the database.

	$ export FLASK_CONFIG=production
	$ export FLASK_APP=run.py
	$ export SQLALCHEMY_DATABASE_URI='mysql://{USER-WITH-FULL-DB-ACCESS}:{USER-PASSWORD}@{SERVER-ADDRESS}/{DB-NAME}'
	$ flask db init
	$ flask db migrate
	$ flask db upgrade

Create a site administrator user.

	$ flask shell
	>>> from app.models import BbUser
	>>> from app import db
	>>> admin = BbUser(email="admin@admin.com",username="admin",password="admin2016",is_admin=True)
	>>> db.session.add(admin)
	>>> db.session.commit()

Create a wsgi.py in the project root folder and write the code below.

	import os
	import sys

	path = '/home/your-username/your-project-directory-name'
	if path not in sys.path:
	    sys.path.append(path)

	os.environ['FLASK_CONFIG'] = 'production'
	os.environ['SECRET_KEY'] = 'YOUR-VERY-OWN-SECRET-KEY'
	os.environ['SQLALCHEMY_DATABASE_URI'] = 'mysql://your-username:your-password@your-host-address/your-database-name'

	from run import app as application

Create uWGSI configuration file with the name 'babel.ini'.

	[uwsgi]
	module = wsgi

	master = true
	processes = 5

	socket = babel.sock
	chmod-socket = 660
	vacuum = true

	die-on-term = true

Create a systemd unit file 'babel.service' within the /etc/systemd/system directory

	[Unit]
	Description=uWSGI instance to serve babel
	After=network.target

	[Service]
	User=user-name
	Group=www-data
	WorkingDirectory=/home/user-name/babel
	Environment="PATH=/home/user-name/babel/babelENV/bin"
	ExecStart=/home/user-name/babel/babelENV/bin/uwsgi --ini babel.ini

	[Install]
	WantedBy=multi-user.target

Start the service and enable it.

	$ sudo systemctl start babel
    $ sudo systemctl enable babel

Create a new server block configuration file for Nginx.

	$ sudo nano /etc/nginx/sites-available/babel

Configure Nginx to proxy requests.

	server {
	    listen 80;
	    server_name server_domain_or_IP;

	    location / {
	        include uwsgi_params;
	        uwsgi_pass unix:/home/user-name/babel/babel.sock;
	    }
	}

Enable Nginx server block configuration and restart Nginx.

	$ sudo ln -s /etc/nginx/sites-available/babel /etc/nginx/sites-enabled
	$ sudo systemctl restart nginx

Thats it!

## Built With

* [Python 3.x](https://docs.python.org/3/)
* [Flask](http://flask.pocoo.org/) - Python microframework

## Author

* **Antonio Ponzano** - [ajponzano](https://gitlab.com/ajponzano)