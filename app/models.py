# app/models.py

from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, login_manager

class BbUser(UserMixin, db.Model):
    """
    Create an user table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'bbusers'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(60), index=True, unique=True)
    username = db.Column(db.String(60), index=True, unique=True)
    first_name = db.Column(db.String(60), index=True)
    last_name = db.Column(db.String(60), index=True)
    password_hash = db.Column(db.String(128))
    is_admin = db.Column(db.Boolean, default=False)

    @property
    def password(self):
        """
        Prevent pasword from being accessed
        """
        raise AttributeError('password is not a readable attribute.')

    @password.setter
    def password(self, password):
        """
        Set password to a hashed password
        """
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<BbUser: {}>'.format(self.username)

# Set up user_loader
@login_manager.user_loader
def load_user(user_id):
    return BbUser.query.get(int(user_id))

class Book(db.Model):
    """
    Create a book table
    """

    __tablename__ = 'books'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text, nullable=False)
    genre = db.Column(db.String(96), nullable=False, index=True)
    #genres = db.relationship('Genre', backref='book', lazy='dynamic')
    author = db.Column(db.String(780), index=True)
    publisher = db.Column(db.String(108), index=True)
    location = db.Column(db.String(10), nullable=False)
    comment = db.Column(db.String(120))

    def __repr__(self):
        return '<Book: {}>'.format(self.title)