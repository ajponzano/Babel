# app/books/__init__.py

from flask import Blueprint

books = Blueprint('books', __name__)

from . import views