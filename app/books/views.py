# app/books/views.py

from flask import abort, flash, redirect, render_template, url_for
from flask_login import current_user, login_required

from . import books
from .forms import SearchForm
from .. import db
from ..models import Book

@books.route('/search', methods=['GET', 'POST'])
def search():
    """
    Search the database
    """
    form = SearchForm()

    if form.validate_on_submit():
        try:
            # search database
            question = '%'+form.title.data+'%'

            if form.searchby.data == '0':
                search_results = Book.query.filter(Book.title.ilike(question))
            elif form.searchby.data == '1':
                search_results = Book.query.filter(Book.genre.ilike(question))
            elif form.searchby.data == '2':
                search_results = Book.query.filter(Book.author.ilike(question))
            elif form.searchby.data == '3':
                search_results = Book.query.filter(Book.publisher.ilike(question))
            elif form.searchby.data == '4':
                search_results = Book.query.filter(Book.location.ilike(question))
            elif form.searchby.data == '5':
                search_results = Book.query.filter(Book.comment.ilike(question))

            flash('Busqueda exitosa.')
        except:
            # something goes wrong
            flash('Error: something went wrong.')

        # redirect to results page
        return list_results(search_results)

    return render_template('books/search.html', action="Search", form=form,
                           title="Busqueda")

def list_results(search_results):
    """
    List search results
    """
    return render_template('books/search_results.html',
                           search_results=search_results,
                           title="Resultados de busqueda")

@books.route('/books/view/<int:id>', methods=['GET'])
def single_view(id):
    """
    Single results view
    """
    book = Book.query.get_or_404(id)

    title_is_abridged = False
    author_is_abridged = False
    title_abridged = book.title
    author_abridged = book.author

    if len(title_abridged) > 24:
        title_abridged = title_abridged[:24] + '...'
        title_is_abridged = True

    if len(author_abridged) > 30:
        author_abridged = author_abridged[:30] + '...'
        author_is_abridged = True

    return render_template('books/single_view.html',
                           book=book,
                           title_is_abridged=title_is_abridged,
                           author_is_abridged=author_is_abridged,
                           title_abridged=title_abridged,
                           author_abridged=author_abridged,
                           title="Vista singular")