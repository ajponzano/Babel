# app/books/forms.py

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField
from wtforms.validators import DataRequired

class SearchForm(FlaskForm):
    """
    Form to search the database
    """
    searchby = SelectField('Buscar por ', choices=[('0','Título'),('1','Género'),
                                       ('2','Autor'),('3','Editorial'),
                                       ('4','Ubicación'),('5','Comentario')])
    title = StringField('', validators=[DataRequired()])
    submit = SubmitField('Buscar')