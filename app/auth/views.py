# app/auth/views.py

from flask import flash, redirect, render_template, url_for
from flask_login import login_required, login_user, logout_user

from . import auth
from .forms import LoginForm
from .. import db
from ..models import BbUser

@auth.route('/login', methods=['GET', 'POST'])
def login():
    """
    Handle requests to the /login route
    Log an user in through the login form
    """
    form = LoginForm()
    if form.validate_on_submit():

        # check whether user exists in the database and whether
        # the password entered matches the password in the database
        bbuser = BbUser.query.filter_by(username=form.username.data).first()
        if bbuser is not None and bbuser.verify_password(
                form.password.data):
            # log user in
            login_user(bbuser)

            # redirect to the appropriate dashboard page after login
            if bbuser.is_admin:
                return redirect(url_for('home.admin_dashboard'))
            else:
                return redirect(url_for('home.dashboard'))

        # when login details are incorrect
        else:
            flash('Nombre de usuario o contraseña invalida.')

    # load login template
    return render_template('auth/login.html', form=form, title='Ingresar')

@auth.route('/logout')
@login_required
def logout():
    """
    Handle requests to the /logout route
    Log an user out through the logout link
    """
    logout_user()
    flash('Se ha cerrado la sesión exitosamente.')

    # redirect to the login page
    return redirect(url_for('auth.login'))