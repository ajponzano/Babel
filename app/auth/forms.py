# app/auth/forms.py

from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField, ValidationError
from wtforms.validators import DataRequired, Email, EqualTo

from ..models import BbUser

class RegistrationForm(FlaskForm):
    """
    Form for users to create new account
    """
    email = StringField('Email', validators=[DataRequired(), Email()])
    username = StringField('Nombre de usuario', validators=[DataRequired()])
    first_name = StringField('Nombre', validators=[DataRequired()])
    last_name = StringField('Apellido', validators=[DataRequired()])
    password = PasswordField('Contraseña', validators=[
                                        DataRequired(),
                                        EqualTo('confirm_password')
                                        ])
    confirm_password = PasswordField('Confirmar contraseña')
    submit = SubmitField('Registrarse')

    def validate_email(self, field):
        if BbUser.query.filter_by(email=field.data).first():
            raise ValidationError('El email ya está en uso.')

    def validate_username(self, field):
        if BbUser.query.filter_by(username=field.data).first():
            raise ValidationError('El nombre de usuario ya está en uso.')

class LoginForm(FlaskForm):
    """
    Form for users to login
    """
    username = StringField('Nombre de usuario', validators=[DataRequired()])
    password = PasswordField('Contraseña', validators=[DataRequired()])
    submit = SubmitField('Ingresar')