# app/admin/forms.py

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

class BookAddForm(FlaskForm):
    """
    Form for admin to add or edit a book
    """
    title = StringField('Título', validators=[DataRequired()])
    genre = StringField('Género', validators=[DataRequired()])
    author = StringField('Autor')
    publisher = StringField('Editorial')
    location = StringField('Ubicación', validators=[DataRequired()])
    comment = StringField('Comentario')
    submit = SubmitField('Agregar')

class BookEditForm(FlaskForm):
    """
    Form for admin to add or edit a book
    """
    title = StringField('Título', validators=[DataRequired()])
    genre = StringField('Género', validators=[DataRequired()])
    author = StringField('Autor')
    publisher = StringField('Editorial')
    location = StringField('Ubicación', validators=[DataRequired()])
    comment = StringField('Comentario')
    submit = SubmitField('Editar')