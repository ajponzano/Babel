# app/admin/views.py

from flask import abort, flash, redirect, render_template, url_for
from flask_login import current_user, login_required

from . import admin
from .forms import BookAddForm, BookEditForm
from .. import db
from ..models import Book, BbUser

def check_admin():
    """
    Prevent non-admins from accessing the page
    """
    if not current_user.is_admin:
        abort(403)

# Book Views

@admin.route('/books', methods=['GET', 'POST'])
@login_required
def list_books():
    """
    List all books
    """
    check_admin()

    books = Book.query.all()

    return render_template('admin/books/books.html',
                           books=books, title="Libros")

@admin.route('/books/add', methods=['GET', 'POST'])
@login_required
def add_book():
    """
    Add a book to the database
    """
    check_admin()

    add_book = True

    form = BookAddForm()
    if form.validate_on_submit():
        book = Book(title=form.title.data, genre=form.genre.data,
                    author=form.author.data, publisher=form.publisher.data,
                    location=form.location.data, comment=form.comment.data)
        try:
            # add book to the database
            db.session.add(book)
            db.session.commit()
            flash('Se agregó un libro exitosamente.')
        except:
            # something goes wrong
            flash('Error: something went wrong.')

        # redirect to books page
        return redirect(url_for('admin.list_books'))

    # load book template
    return render_template('admin/books/book.html', action="Add",
                           add_book=add_book, form=form,
                           title="Agregar Libro")

@admin.route('/books/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_book(id):
    """
    Edit a book
    """
    check_admin()

    add_book = False

    book = Book.query.get_or_404(id)
    form = BookEditForm(obj=book)
    if form.validate_on_submit():
        book.title = form.title.data
        book.genre = form.genre.data
        book.author = form.author.data
        book.publisher = form.publisher.data
        book.location = form.location.data
        book.comment = form.comment.data
        db.session.commit()
        flash('Se editó el libro correctamente.')

        # redirect to the books page
        return redirect(url_for('admin.list_books'))

    form.comment.data = book.comment
    form.location.data = book.location
    form.publisher.data = book.publisher
    form.author.data = book.author
    form.genre.data = book.genre
    form.title.data = book.title
    return render_template('admin/books/book.html', action="Edit",
                           add_book=add_book, form=form,
                           book=book, title="Editar Libro")

@admin.route('/books/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_book(id):
    """
    Delete a book from the database
    """
    check_admin()

    book = Book.query.get_or_404(id)
    db.session.delete(book)
    db.session.commit()
    flash('Se borró el libro correctamente.')

    # redirect to the books page
    return redirect(url_for('admin.list_books'))

    return render_template(title="Borrar Libro")

# User Views

@admin.route('/users')
@login_required
def list_users():
    """
    List all users
    """
    check_admin()

    users = BbUser.query.all()
    return render_template('admin/users/users.html',
                           users=users, title='Usuarios')