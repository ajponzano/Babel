# app/home/views.py

from flask import abort, flash, redirect, render_template, url_for
from flask_login import current_user, login_required

from . import home
from .. import db
from ..models import Book

@home.route('/')
def homepage():
    """
    Render the homepage template on the / route
    """
    book_count = Book.query.count()

    return render_template('home/index.html', book_count=book_count,
                           title="Bienvenido")

@home.route('/admin/dashboard')
@login_required
def admin_dashboard():
    # prevent non-admins from accessing the page
    if not current_user.is_admin:
        abort(403)

    return render_template('home/admin_dashboard.html', title="Tablero")